from bs4 import BeautifulSoup
import cookielib
import socks
import socket

url = 'https://www.ip.biz.tr/'

def create_connection(address, timeout=None, source_address=None):
    sock = socks.socksocket()
    sock.connect(address)
    return sock

socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)

# patch the socket module
socket.socket = socks.socksocket
socket.create_connection = create_connection

import urllib2

req = urllib2.Request(url)
try:
    response = urllib2.urlopen(req)
except urllib2.HTTPError, e:
    print e.fp.read()
the_page = response.read()
soup = BeautifulSoup(the_page,'html.parser')
print soup.find_all(class_='fg-blue')[0].get_text()
