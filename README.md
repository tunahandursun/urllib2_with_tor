# urllib2_with_tor

  This project is example for urllib2 usage with tor.
  "there_is_no_tor.py"  has got no tor configuration. This is only gets ip address from https://www.ip.biz.tr/
  "urllib2_with_tor.py" has got tor configuration and gets ip address from https://www.ip.biz.tr/

# Requirements

 You must install python-socks, python-bs4 and tor package.
# Notes

 You should start tor service(systemctl start tor).

Thanks for https://stackoverflow.com/questions/5620263/using-an-http-proxy-python, BeautifulSoup authors and Guido van Rossum :)
